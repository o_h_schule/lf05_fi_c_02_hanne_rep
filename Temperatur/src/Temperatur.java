
public class Temperatur {

	public static void main(String[] args) {
		System.out.printf("%s %2s %5s \n", "Fahrenheit", "|", "Celsius");
		System.out.printf("- - - - - - - - - - - - \n");
		System.out.printf("%d %9s %7.2f \n", -20, "|", -28.89);
		System.out.printf("%d %9s %7.2f \n", -10, "|", -23.33);
		System.out.printf("%s %10s %7.2f \n", "+0", "|", -17.78);
		System.out.printf("%s %9s %7.2f \n", "+20", "|", -6.67);
		System.out.printf("%s %9s %7.2f \n", "+30", "|", -1.11);

	}

}
