﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       // Änderung des Datentypen zu BigDecimal
       // 
       // =======================================
       while(true) 
       {
    	   double zuZahlen = fahrkartenbestellungErfassen();
           double GesamtB = fahrkartenBezahlen(zuZahlen);
           fahrkartenAusgeben();
           rueckgeldAusgeben(GesamtB, zuZahlen);
       }
       
    }
    public static double fahrkartenbestellungErfassen()
    {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int ticketType;
    	int kartenMultiplikator;
    	
    	String[] fahrkartenTyp= {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenTypKosten= {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	// Länge überprüfen
    	if(fahrkartenTyp.length != fahrkartenTypKosten.length) {
    		System.out.print("Die Länge der Eingabearrays stimmt nicht überein!");
    		System.exit(1);
    	}
    	
    	// Welche Fahrkartenart?
    	System.out.print("Wählen Sie ihre Wunschfahrkarte aus:\n");
    	for(int i = 0 ; i != fahrkartenTyp.length ; i++) {
    		System.out.printf("%s (%.2f)[%d]\n", fahrkartenTyp[i], fahrkartenTypKosten[i], i+1);
    	}    		
    	ticketType = tastatur.nextInt();
    	
    	// Auswählen und setzen der Kosten für einzelne Karte
    	zuZahlenderBetrag = fahrkartenTypKosten[ticketType-1];
    		
    	// Wie viele Karten sollen gezogen werden?
        System.out.print("Zahl der Karten: ");
        kartenMultiplikator = tastatur.nextInt();
        
        // Final
        zuZahlenderBetrag = kartenMultiplikator * zuZahlenderBetrag;
        
        return zuZahlenderBetrag;
    }
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
    	
    	// Geldeinwurf
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {   
     	   double currentBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f \n", currentBetrag);
     	   System.out.print("Ich gebe... (mind. 5 Cent, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }
    public static void fahrkartenAusgeben()
    {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
    {
    	double rückgabebetrag;
    	
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
              muenzeAusgeben(2, "EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
              muenzeAusgeben(1, "EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
              muenzeAusgeben(50, "CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
              muenzeAusgeben(20, "CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
              muenzeAusgeben(10, "CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
              muenzeAusgeben(5, "CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    public static void warte(int millisekunde)
    {
    	try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public static void muenzeAusgeben(int betrag, String einheit)
    {
    	System.out.printf("%d %s%n", betrag, einheit);
    }
}