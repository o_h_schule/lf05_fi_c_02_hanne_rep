import java.util.Scanner;

public class Sortieren {
	public static void main(String[] args) {
	Scanner tastatur = new Scanner(System.in);
	char[] x;
	
	System.out.print("3 Chars eingeben: ");
	x = tastatur.nextLine().toCharArray();
	
	min(x);
	System.out.print("Bubble-sorted your chars for your! = " + compare(x));
	
	}
	static void min(char[] x) {
		if (x[0] < x[1] && x[0] < x[2])
			System.out.print("Smallest char = " + x[0] + "\n");
		else if (x[1] < x[0] && x[1] < x[2])
			System.out.print("Smallest char = " + x[1] + "\n");
		else 
			System.out.print("Smallest char = " + x[2] + "\n");
	}
	static String compare(char[] x) {
		char y;
        for (int i = 0; i < x.length - 1; i++) {
            if (x[i] < x[i + 1]) {
                continue;
            }
            y = x[i];
            x[i] = x[i + 1];
            x[i + 1] = y;
            compare(x);
        }
        return String.valueOf(x);
	}
	
}
