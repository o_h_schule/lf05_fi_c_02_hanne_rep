import java.util.Scanner;

public class Schaltjahr {
	public static void main(String[] args) 
	{
		Scanner tastatur = new Scanner(System.in);
		int jahr = 0;
		
		System.out.print("Ist dieses Jahr ein Schaltjahr?: ");
		jahr = tastatur.nextInt();
		
		checkLeap(jahr);
	}
	public static void checkLeap(int jahr) 
	{
		if (jahr <= 1582) 
		{
			System.out.print(jahr + " liegt vor der Einführung des Schaltjahres!\n");
			if (jahr % 4 == 0) 
			{
				System.out.print("Schaltjahr!");
			}
		}
		else if (jahr % 4 == 0 && jahr % 100 != 0) 
		{
			System.out.print("Schaltjahr!");
		} 
		else 
		{
			if (jahr % 400 == 0) 
			{
				System.out.print("Schaltjahr!");
			}
			else 
			{
				System.out.print("Kein Schaltjahr!");
			}
		}
	}
}
