import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		int u;
		String[] arrayOfMonths = {"Januar","Februar", "Maerz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"}; 
		
		System.out.print("Monat numerisch eingeben: ");
		u = tastatur.nextInt();
		
		printMonthString(u, arrayOfMonths);
		printMonthSwitch(u);
		printMonthIf(u);
	}
	
	static void printMonthString(int u, String[] arrayOfMonths) {
		System.out.print(arrayOfMonths[u-1]);
	}
	static void printMonthSwitch(int u) {
		switch(u) {
		
		case 1:
			System.out.print("Januar");
			break;
		case 2:
			System.out.print("Februar");
			break;
		case 3:
			System.out.print("M�rz");
			break;
		case 4:
			System.out.print("April");
			break;
		case 5:
			System.out.print("Mai");
			break;
		case 6:
			System.out.print("Juni");
			break;
		case 7:
			System.out.print("Juli");
			break;
		case 8:
			System.out.print("August");
			break;
		case 9:
			System.out.print("September");
			break;
		case 10:
			System.out.print("Oktober");
			break;
		case 11:
			System.out.print("November");
			break;
		case 12:
			System.out.print("Dezember");
			break;
		}
	}
	static void printMonthIf(int u) {
		// soon
	}

}
