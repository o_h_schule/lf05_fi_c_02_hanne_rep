import java.util.Scanner;

public class Raviolita {
	public static void main (String[] args) {
		Scanner tastatur = new Scanner(System.in);
		final double PI = 3.141592;
		double u,h;
		
		System.out.print("Umfang der Konserve: ");
		
		u = tastatur.nextInt();
		
		System.out.print("H�he der Konserve: ");
		
		h = tastatur.nextInt();
		
		//auto-run methods
		double durchm = durchmesser(u,h,PI);
		bodenflaeche(u,h,PI, durchm);
	}
	static double durchmesser(double u, double h, double PI) {
		double durchm;
		durchm = u/((PI)*(PI));
		System.out.printf("Durchmesser: %.2f\n", durchm);
		return durchm;
	}
	static void bodenflaeche(double u, double h, double PI, double durchm) {
		double bodenflaeche;
		bodenflaeche = PI*((durchm/2)*(durchm/2)); 
		System.out.printf("Fl�che des Bodens: %.2f\n", bodenflaeche);
	}
	
}